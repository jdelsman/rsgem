# -*- coding: utf-8 -*-
"""
Created on Fri May 02 15:10:39 2014

@author: delsman

RR-model calculating salt load to sw
Steps: 
- iterate Qout with head change
- calculate flux density (Groenendijk & Eertwegh 2004)
- calculate zeta based on mass balance
- calculate concentrations based on flux density and zeta
- infiltration surface water possible
- unsaturated zone according to Brauer et al 2014
"""

import numpy as np
import pandas as pd
import time
from scipy.optimize import minimize_scalar
from scipy.integrate import quad
from scipy.interpolate import interp1d
from copy import deepcopy

def ani_transform(P):
    if 'ani' in P and 'ani_transformed' not in P:
        P['drain_L'] *= (1./P['ani'])**0.5
        P['ditch_L'] *= (1./P['ani'])**0.5
        P['drain_K'] = (P['drain_K']**2/P['ani'])**0.5
        P['ditch_K'] = (P['ditch_K']**2/P['ani'])**0.5
        P['ani_transformed'] = True
        P['ani_q_fct'] = 1. / (1./P['ani'])**0.5
    elif 'ani_q_fct' not in P:
        P['ani_q_fct'] = 1.
    return P

def Hooghoudt(K, dh, L, b, D, ani_fct=1.):
    Deff = D / (1 + 8*D/np.pi/L*np.log(D/(b+2*.1)))
    if dh>=0:
        Q = (8*K*Deff*dh+4*K*dh**2)/L**2
    else:
        Q = (8*K*Deff*dh-4*K*dh**2)/L**2
    return -Q * ani_fct * 1000.,Deff  # to mm/d

def Qh(h,wl,param,tstep):
    OUT_DR,deff = Hooghoudt(param['drain_K'],max(0.,h-max(param['drain_level'],wl)),param['drain_L'],param['drain_b'],50,param['ani_q_fct'])
    
    wl = max(wl,param['ditch_bottom'])
    mu_di = h-wl
    if wl == param['ditch_bottom'] or ('ditch_infil' in param and not param['ditch_infil']):
        mu_di = max(0.,mu_di)
    OUT_DI,deff = Hooghoudt(param['ditch_K'],mu_di,param['ditch_L'],param['ditch_b'],50,param['ani_q_fct'])
    
    return OUT_DR*tstep/86400.,OUT_DI*tstep/86400.  # to mm/tstep

def FluxTimestep(flux,tstep,torg):
    flux_t = {}
    for k in flux.keys(): # boundaries
        if 'IN_' in k:        
            flux_t[k] = flux[k] * tstep/float(torg)
        else:
            flux_t[k] = flux[k]
    return flux_t

def AddFlux(flux_t,flux):
    '''Add sub-tstep flux to flux'''
    if flux is None:
        flux = {k:0. for k in flux_t.keys()}
    
    for k in flux_t.keys():
        flux[k] += flux_t[k]
    
    return flux
        
def Boundaries(h,bound,param,tstep):
    flux = {}
    for k in bound.keys(): # boundaries
        if 'EVT' in k:
            surf = param['BND_EVT_surf']
            exdp = param['BND_EVT_exdp']
            k2 = 'IN'+k[k.index('_'):]
            flux[k2] = bound[k]*min(1.,max(0.,(h-surf-exdp)/exdp))
        if k+'_c' in param:  # cauchy boundary
            k2 = 'IN'+k[k.index('_'):]
            flux[k2] = (bound[k]-h) / param[k+'_c'] *1000/86400*tstep  #m/d --> mm/tstep
        if 'IN_' in k:
            flux[k] = bound[k] * tstep/float(param['tstep'])
    return flux

#@np.vectorize
def EqStorageDeficit(dg,param):
    '''Calculate equilibrium storage deficit for a given groundwater depth'''
    binv = 1./param['unsat_b']
    return param['unsat_thetas']*(dg - ( dg**(1.-binv) / ((1.-binv)*param['unsat_psiae']**(-binv)) ) - 
                                   ( param['unsat_psiae']/(1.-param['unsat_b']) ) )

def min_EqStorDef(dg,dvt,param):
    ##### TODO: include capillary fringe
    dv = Brooks_Corey_int(dg,param)
    return abs(dv-dvt)

def Brooks_Corey(h,param):
    '''Calculate storage deficit for a given h above groundwater table'''
    return param['unsat_thetas'] - (param['unsat_thetas']-0.005) * min(1., (h / float(param['unsat_psiae']))**(-1./param['unsat_b']))

def Brooks_Corey_int(dV,h,param):
    ''' really crude... walk until found'''
    
    x = np.linspace(param['unsat_psiae'],1000.*(param['surf_elev']-h),500)
    for i in range(len(x)):
        #if quad(Brooks_Corey,param['unsat_psiae'],x[i],args=(param))[0] > dV:
        if EqStorageDeficit(x[i],param) > dV:
            break
        
    return x[i]
    
def GwlFromStorDef(stordef,P,h,param,tstep):
    '''Calculate groundwater level from difference between actual and equilibrium storage deficit'''
    #dstordef = min(dstordef, 0.) # fast route only when adding water
    stordef_slow = stordef# + (1. - param['f_macropore']) * dstordef
    slow_comp = float(tstep)*((stordef_slow - EqStorageDeficit((param['surf_elev'] - h) * 1000., param)) / (param['unsat_Cv']*86400))  # mm water from / to unsat zone
    fast_comp = min(0., param['f_macropore'] * -P)
    #print 'h:',h,dstordef, slow_comp,fast_comp
        
    # find head rise from the equilibrium profile, lowering from specific yield
    dV = fast_comp+slow_comp
    '''dh = Brooks_Corey_int(abs(dV),h,param)  
    if dV < 0:
        #print stordef, P, fast_comp, slow_comp
        #res = minimize_scalar(min_EqStorDef,args=(-dV,param),bounds=(param['unsat_psiae'],1000.*(param['surf_elev'] - h)))  # fill from below...
        
        #if not capfr:
            # fill capillary fringe:
            
        
        #print res.x, h + res.x / 1000.
        return h + dh / 1000.
    else:
        return h - dh / 1000.# / param['spec_yield']
    '''
    return h - dV / 1000. / param['spec_yield']
    
    #### TODO: Macropore flow?
    #return h - param['spec_yield']*tstep*((stordef - EqStorageDeficit(h, param)) / (param['unsat_Cv']*86400)) / 1000.

def WaterBalance_expl(bound,wl,state,param,tstep):
    '''Calculate water balance explicitly'''    
    # fluxes:
    flux_t = Boundaries(state['head'],bound,param,tstep)
    flux_t['OUT_DR'],flux_t['OUT_DI'] = Qh(state['head'],wl,param,tstep)

    # waterbalance:
    flux_t['STOR'] = -sum(flux_t.values()) # storage change
    
    # update states
    state_t = {}#deepcopy(state)
    state_t['stordef'] = state['stordef'] + flux_t['STOR']
    #print 1,state['head'], state_t['stordef']
    state_t['head'] = GwlFromStorDef(state_t['stordef'],flux_t['IN_P'],state['head'],param,tstep)
    #print 2,state_t['head']
    
    # ponding? To overland flow
    if state_t['stordef'] < 0.:
        #print 'ponding'
        flux_t['OUT_OF'] = state_t['stordef']
        state_t['stordef'] = 0.
        state_t['head'] = param['surf_elev']
    else:
        flux_t['OUT_OF'] = 0.
        
    return state_t, flux_t

    
def WaterBalance_min(stordef,flux,bound,wl,state,param,minimize=True):
    # ponding? To overland flow
    flux['OUT_OF'] = min(0., stordef)
    stordef = max(0., stordef)
    
    # first: change in groundwater level
    h = GwlFromStorDef(stordef,state['head'],param)
    # fluxes:
    flux['OUT_DR'],flux['OUT_DI'] = Qh((h+state['head'])/2.,wl,param)
    flux.update(Boundaries(h,bound,param))
    flux['STOR'] = stordef - state['stordef'] # storage change
    
    # waterbalance
    if minimize:
        return abs(sum(flux.values()))
    else:
        #print flux, sum(flux.values())
        # update state
        statet = deepcopy(state)
        statet['stordef'] = stordef
        statet['head'] = h
        # return state and fluxes
        return statet, flux


def model_tstep(bound={},state={},param={}):
    '''Model one time step and save results'''
    # init stordef if not done already:
    if 'stordef' not in state or state['stordef'] is None:
        state['stordef'] = EqStorageDeficit((param['surf_elev']-state['head'])*1000.,param)    

    # store previous state    
    statetm1 = deepcopy(state)
    
    # Get wl from boundary conditions or param
    wl = bound.pop('BND_DILEV',param['ditch_level']) # retrieve water level from boundcond, otherwise param
    
    # assert anisotropy correction:
    if 'ani' in param and 'ani_transformed' not in param:
        param = ani_transform(param)

    # Calculate water balance explicitly, decrease timestep if necessary
    tstep = param['tstep']
    stepOK = False
    while not stepOK:
        flux = None # initialize flux
        stepOK = True
        n = int(param['tstep'] / tstep)
        for t in range(n):
            state_t, flux_t = WaterBalance_expl(bound,wl,state,param,tstep)
            #print state_t, flux_t
            # check flux and state: timestep too large?
            if (tstep > param['min_tstep'] and 
                (flux_t['IN_P'] > param['crit_P'] or 
                abs(state['head']-state_t['head']) > param['crit_dh'])):
                    stepOK = False
                    #print 'decreasing tstep'        
                    tstep = max(param['min_tstep'], tstep / float(param['fct_tstep']))
                    tstep = param['tstep'] / float(int(param['tstep'] / tstep))
                    #print tstep, flux_t['IN_P'], abs(state['head']-state_t['head'])
                    # reset state
                    state = deepcopy(statetm1)
                    break
            else:
                # update state and flux
                state.update(state_t)
                flux  = AddFlux(flux_t,flux)
                #print state, flux, sum(flux.values())
    
   
    #res = minimize_scalar(WaterBalance,args=(flux,bound,wl,state,param,True),tol=1e-5)
    #print res
    #state, flux = WaterBalance(res.x,flux,bound,wl,statetm1,param,False)
    #print state, statetm1
#    state['head'] = res.x
#    flux['OUT_DR'],flux['OUT_DI'],state['mu_di'] = Qh((state['head']+statetm1['head'])/2.,wl,param)
#    flux.update(Boundaries(state['head'],bound,param))
#    flux['STOR'] = -1000.*param['spec_yield']*(state['head']-statetm1['head']) # in mm
    
    # Evaluate zeta: how much of flow to drain / ditch is above / below zeta?
    # eq 24 from Groenendijk&Eertwegh 2004. Eq 24 calculates qy, but qy = 1 - integral(qx dy)
    # evaluation of eq 24 at y = zeta therefore gives total qx below zeta surface
    y = min(state['zeta'],state['head']) - state['head']
    fz_dr = (2./np.pi*np.arctan(np.exp(2.*np.pi*y/param['drain_L'])/
                (1-np.exp(4*np.pi*y/param['drain_L']))**0.5))
    fz_di = (2./np.pi*np.arctan(np.exp(2.*np.pi*y/param['ditch_L'])/
                (1-np.exp(4*np.pi*y/param['ditch_L']))**0.5))
        
    # fluxes above and below (are equal-sized..?)
    above = flux['IN_P'] + (1.-fz_dr)*flux['OUT_DR'] + (1.-fz_di)*flux['OUT_DI'] + flux['STOR']
    below = flux['IN_SEEP'] + fz_dr*flux['OUT_DR'] + fz_di*flux['OUT_DI']

    state['zeta'] = min(state['head'], state['zeta'] + below / 1000. / param['porosity'])
    #zeta_avg = 0.5*state['zeta']+0.5*statetm1['zeta']
    
    conc = {'OUT_DR':np.nan,'OUT_DI':np.nan}
    flux['OUT_DI_INFIL'] = 0
    if flux['OUT_DR']:
        conc['OUT_DR'] = ((1.-fz_dr)*flux['OUT_DR']*param['precip_conc'] + fz_dr*flux['OUT_DR']*param['seep_conc'])/flux['OUT_DR']
    
    # evaluate infiltrated water:
    # store infiltrated amount and infiltration height (wl at time of infiltration)
    if flux['OUT_DI'] > 0:
        state['infil'] += flux['OUT_DI']
        state['infil_height'] = (flux['OUT_DI']*wl+statetm1['infil']*statetm1['infil_height'])/state['infil']
    elif state['infil'] > 0:
        y = min(state['infil_height'],state['head']) - state['head']
        fi_di = (2./np.pi*np.arctan(np.exp(2.*np.pi*y/param['ditch_L'])/
                    (1-np.exp(4*np.pi*y/param['ditch_L']))**0.5))
        flux['OUT_DI_INFIL'] = max(-state['infil'],fi_di*flux['OUT_DI'])
        state['infil'] += flux['OUT_DI_INFIL']
               
        # calculate remaining contribution of deep and shallow exf
        fiz = min(fz_di / fi_di,1.)
        flux_di_d = fz_di * flux['OUT_DI'] - fiz*flux['OUT_DI_INFIL']
        flux_di_s = (1-fz_di) * flux['OUT_DI'] - (1.-fiz)*flux['OUT_DI_INFIL']
        # determine concentration
        conc['OUT_DI'] = (flux['OUT_DI_INFIL']*param['infil_conc'] + 
                          flux_di_s*param['precip_conc'] + 
                          flux_di_d*param['seep_conc'])/flux['OUT_DI']
    else:
        conc['OUT_DI'] = ((1.-fz_di)*flux['OUT_DI']*param['precip_conc'] + fz_di*flux['OUT_DI']*param['seep_conc'])/flux['OUT_DI']
    
    
    return flux,conc,state,statetm1,n

def run_rsgem(boundcond,state,param,verbose=False):
    '''Run RSGEM for all timesteps in boundcond,
    with starting_state state, and model parameters param
    
    Return pandas DataFrame'''
    if verbose: 
        t1 = time.time()
        print( 'Running RSGEM...',)

    # make sure no nodata in boundcond
    boundcond = boundcond.fillna(method='pad')
    
    # apply anisotropy transformation
    param = ani_transform(param)
    
    # run model for consecutive time steps
    result = []
    for i,boundcond_t in boundcond.iterrows():
        flux,conc,state,statetm1,n = model_tstep(bound=boundcond_t.to_dict(),state=state,param=param)
        result += [[state['stordef'],state['head'],state['zeta'],state['infil'],-flux['OUT_DR'],-flux['OUT_DI'],-flux['OUT_OF'],flux['IN_SEEP'],flux['STOR'],conc['OUT_DR'],conc['OUT_DI'],n]]
    
    if verbose: 
        t2 = time.time()
        print ('done, in %i sec'%int(t2-t1))

    # return dataframe of results    
    return pd.DataFrame(result,index=boundcond.index,columns=['stordef','head','zeta','infil','qdr','qdi','qof','seep','stor','cqdr','cqdi','n'])

# model parameters: FLOATS!!
param = {'porosity': .35,
         'spec_yield': .10,
         'ani':10.,
         'ditch_K': .25,
         'drain_K': .25,
         'ditch_level': -1.06,
         'ditch_bottom': -1.06,
         'drain_level': -1.,
         'drain_L': 5.,
         'ditch_L': 125.,
         'drain_b': .1,
         'ditch_b': 2.,
         'ditch_infil': False,
         'unsat_b': 2.3,
         'unsat_psiae': 50.,
         'unsat_thetas': 0.485,
         'unsat_Cv': .1,  #[d]
         'f_macropore':0,#.2,
         'BND_SEEP_c':2000.,
         'BND_EVT_surf':-10,
         'BND_EVT_exdp':1,
         'seep_conc':21,
         'precip_conc':1.,
         'infil_conc':2.,
         'surf_elev':0,
         'area': 125. * 35.,
         'tstep':86400.,
         'min_tstep':100.,
         'fct_tstep':10,
         'crit_P':5.,
         'crit_dh':0.01
         }
# model state          
state = {'stordef':None,
         'head': -1.,
         'zeta': -1.5,
         'capfr': False,
         'infil': 0,
         'infil_height':-1.
         }

# 1. Calculate waterbalance, head and flux to drains and ditch
flux = {'IN_P': 5.,
        'IN_SEEP': 0.3
        }



if __name__ == '__main__':
    import knmi
    import matplotlib.pyplot as plt

    fn_knmidata = knmi.download_knmidata(stn=235,mtype='d',start='20000101',
                                         end='20110101',mvars=['RH','EV24'],
                                         fname='knmidata.dat')
    mdata = knmi.read_knmidata('knmidata.dat')
    rech = (mdata['RH'] - mdata['EV24']) / 10.
    seepd = rech*0.+.3
    flux_in = pd.concat((rech,seepd),axis=1,keys=['IN_P','IN_SEEP'])
   
    
    param['drain_Deff'] = Hooghoudt(param['drain_K'],0,param['drain_L'],param['drain_b'],50)[1]
    param['ditch_Deff'] = Hooghoudt(param['ditch_K'],0,param['ditch_L'],param['ditch_b'],50)[1]
    
    ## RUNNING MODEL
    #state['stordef'] = EqStorageDeficit(state['head'],param)
#    for i in range(10):
    #flux,c,state,stm1,n = model_tstep(flux,state,param)
    result = run_rsgem(boundcond=flux_in,state=state,param=param,verbose=True)
    
    # plot of head and zeta variations
    result['head'].plot(style='g-')
    result['zeta'].plot(style='r-')
    plt.show()
    
    # plot of drain and ditch discharge
    result['qdr'].plot(style='r-')
    result['qdi'].plot(style='g-') 
    result['infil'].plot(style='b-') 
    plt.show()

    # plot of drain and ditch exfiltration concentration
    result['cqdr'].plot(style='r-')
    result['cqdi'].plot(style='g-')
    plt.show()
    