"""
Adaptation of RSGEM model to work on top of iMOD-WQ results

steps:
- Use imod-wq results as head regional groundwater and groundwater concentration
- Run water balance distributed based on RSGEM model on xr.DataArray of parameters
- Apply RSGEM concepts to calculate position of fresh/salt interface and flux concentrations

How does RSGEM work again?
    eq 24 from Groenendijk&Eertwegh 2004. Eq 24 calculates qy, but qy = 1 - integral(qx dy)
    evaluation of eq 24 at y = zeta therefore gives total qx below zeta surface

    first: get position of zeta, the fresh/salt interface, as min of zeta_t-1 and head
    y = min(state['zeta'],state['head']) - state['head']

    then: for each drainage system: calculate factor of flow that is below zeta
        fz_dr = (2./np.pi*np.arctan(np.exp(2.*np.pi*y/param['drain_L'])/
                    (1-np.exp(4*np.pi*y/param['drain_L']))**0.5))
        fz_di = (2./np.pi*np.arctan(np.exp(2.*np.pi*y/param['ditch_L'])/
                    (1-np.exp(4*np.pi*y/param['ditch_L']))**0.5))

    finally: use these factors to divide waterbalance in part above and below zeta,
    use difference to move zeta up and down
  
"""
import numba
from numba import float32
import numpy as np
import pandas as pd
import xarray as xr
import time
from scipy.optimize import minimize_scalar
from pathlib import Path
from dask.diagnostics import ProgressBar
import matplotlib.pyplot as plt

@numba.njit
def _calc_fz(y,L):
    return (2./np.pi*np.arctan(np.exp(2.*np.pi*y/L)/
                (1-np.exp(4*np.pi*y/L))**0.5))


@numba.njit
def _calculate_rsgem(zeta_start, head, bdgflf, fluxes, L, porosity, conc_rch, conc_gw, out_zeta, out_fluxwconc, out_fluxconc):
    nt,ny,nx = head.shape
    for iy in range(ny):
        for ix in range(nx):
            if np.isnan(conc_gw[iy, ix]) or conc_gw[iy, ix] < 0.1:
                continue
            for it in range(nt):
                if it:
                    zeta_tmin1 = out_zeta[it-1, iy, ix]
                else:
                    zeta_tmin1 = zeta_start[iy, ix]
                
                y = min(zeta_tmin1, head[it, iy, ix]) - head[it, iy, ix]
                fz = _calc_fz(y,L[:, iy, ix])
                below = bdgflf[it, iy, ix] + (fz*fluxes[:, it, iy, ix]).sum()
                out_zeta[it, iy, ix] = min(head[it, iy, ix], zeta_tmin1 + below / 1000. / porosity[iy, ix])
                out_fluxconc[:, it, iy, ix] = fz*conc_gw[iy, ix] + (1-fz)*conc_rch[iy, ix]
                out_fluxwconc[it, iy, ix] = (out_fluxconc[:, it, iy, ix] * fluxes[:, it, iy, ix]).sum() / fluxes[:, it, iy, ix].sum()


#@numba.guvectorize("(), (n), (n), (m,n), (m), (), (), (), () -> (n), (n), (m,n)")
@numba.guvectorize("(float32, float32[:], float32[:], float32[:,:], float32[:], float32, float32, float32, float32, float32[:], float32[:], float32[:,:])", "(), (n), (n), (m,n), (m), (), (), (), () -> (n), (n), (m,n)")
def _calculate_rsgem_guv(zeta_start, head, bdgflf, fluxes, L, porosity, conc_rch, conc_gw, min_zeta, out_zeta, out_fluxwconc, out_fluxconc):
    nt = int(head.shape[0])
    if np.isnan(conc_gw) or conc_gw < 0.1:
        return 
    for it in range(nt):
        if it:
            zeta_tmin1 = out_zeta[it-1]
        else:
            zeta_tmin1 = zeta_start
        
        y = min(zeta_tmin1, head[it]) - head[it]
        fz = _calc_fz(y,L)
        below = bdgflf[it] + (fz*fluxes[:, it]).sum()
        out_zeta[it] = min(head[it], zeta_tmin1 + below / 1000. / porosity)
        out_fluxconc[:, it] = fz*conc_gw + (1-fz)*conc_rch
        out_fluxwconc[it] = (out_fluxconc[:, it] * fluxes[:, it]).sum() / fluxes[:, it].sum()
        if out_zeta[it] < min_zeta:
            return
    return

def calculate_rsgem_lhm(input_mf, L, area, porosity, conc_rch, conc_gw):
    zeta = xr.full_like(input_mf["head"], np.nan).astype(np.float32)
    zeta.name = "zeta"
    fluxes = input_mf[["bdgriv_sys1","bdgriv_sys2","bdgriv_sys3","bdgdrn_sys1","bdgdrn_sys3"]].to_array(dim="sys",name="flux").chunk({"sys":5})
    fluxes = fluxes / area * 1000.   # m3 naar mm
    assert(L.shape[0]==fluxes.shape[0])
    fluxwconc = xr.full_like(input_mf["head"], np.nan).astype(np.float32)
    fluxwconc.name = "fluxwconc"
    fluxconc = xr.full_like(fluxes, np.nan).astype(np.float32)
    fluxconc.name = "conc"
    bdgflf = input_mf["bdgflf"] / area * 1000.   # m3 naar mm
    porosity = xr.full_like(input_mf["head"].isel(time=0), porosity)
    
    zeta_start = input_mf["head"].isel(time=0) - 2.5  # initialize zeta @ 2m below first head
    min_zeta = input_mf["head"].isel(time=0) - 30.
    #_calculate_rsgem(zeta_start, input_mf["head"].values, bdgflf.values, fluxes.values, L.values, porosity.values, conc_rch.values, conc_gw.values, zeta.values, fluxconc.values)
    zeta, fluxwconc, fluxconc = xr.apply_ufunc(_calculate_rsgem_guv, zeta_start.astype(np.float32), input_mf["head"].astype(np.float32), bdgflf.astype(np.float32), fluxes.astype(np.float32), L.astype(np.float32), porosity.astype(np.float32), conc_rch.astype(np.float32), conc_gw.astype(np.float32), min_zeta.astype(np.float32), zeta, fluxwconc, fluxconc, input_core_dims=[[],["time"],["time"],["sys","time"],["sys"],[],[],[],[],["time"],["time"],["sys","time"]], output_core_dims=[["time"],["time"],["sys","time"]],dask="parallelized",output_dtypes=[np.float32,np.float32,np.float32])  # list with one entry per arg
    return xr.Dataset({"zeta":zeta, "fluxwconc":fluxwconc, "fluxconc":fluxconc})

def ani_transform(P):
    if 'ani' in P and 'ani_transformed' not in P:
        P['drain_L'] *= (1./P['ani'])**0.5
        P['ditch_L'] *= (1./P['ani'])**0.5
        P['drain_K'] = (P['drain_K']**2/P['ani'])**0.5
        P['ditch_K'] = (P['ditch_K']**2/P['ani'])**0.5
        P['ani_transformed'] = True
        P['ani_q_fct'] = 1. / (1./P['ani'])**0.5
    elif 'ani_q_fct' not in P:
        P['ani_q_fct'] = 1.
    return P

@numba.njit
def Hooghoudt(K, dh, L, b, D, ani_fct=1.):
    Deff = D / (1 + 8*D/np.pi/L*np.log(D/(b+2*.1)))
    return -np.where(dh>=0,(8*K*Deff*dh+4*K*dh**2)/L**2,
                     (8*K*Deff*dh-4*K*dh**2)/L**2)*ani_fct*1000.,Deff # to mm/d

def Qh(h,wl,param):
    OUT_DR,deff = Hooghoudt(param['drain_K'],
                            max(0.,h-max(param['drain_level'],wl)),
                            param['drain_L'],
                            param['drain_b'],
                            50,
                            param['ani_q_fct'])
    
    wl = max(wl,param['ditch_bottom'])
    mu_di = h-wl
    if (wl == param['ditch_bottom'] or 
        ('ditch_infil' in param and not param['ditch_infil'])):
        mu_di = max(0.,mu_di)
    OUT_DI,deff = Hooghoudt(param['ditch_K'],
                            mu_di,
                            param['ditch_L'],
                            param['ditch_b'],
                            50,
                            param['ani_q_fct'])
    
    return OUT_DR/86400.*param['tstep'],OUT_DI/86400.*param['tstep'],mu_di

def Boundaries(h,bound,param):
    flux = {}
    for k in bound.keys(): # boundaries
        if 'EVT' in k:
            surf = param['BND_EVT_surf']
            exdp = param['BND_EVT_exdp']
            k2 = 'IN'+k[k.index('_'):]
            flux[k2] = bound[k]*min(1.,max(0.,(h-surf-exdp)/exdp))
        if k+'_c' in param:  # cauchy boundary
            k2 = 'IN'+k[k.index('_'):]
            flux[k2] = (bound[k]-h) / param[k+'_c']
            flux[k2] *= 1000/86400*param['tstep']  #m/d --> mm/tstep
    return flux

def wbal(h,flux,bound,wl,state,param):
    OUT_DR,OUT_DI,mu_di = Qh((h+state['head'])/2.,wl,param)
    flux.update(Boundaries(h,bound,param))
    return abs((sum(flux.values())+OUT_DR+OUT_DI) - 
               1000*param['spec_yield']*(h-state['head']))


def model_tstep(boundcond={},state={},param={}):
    '''Model one time step and save results'''
    # Split boundary conditions
    flux = {}
    bound = {}
    # retrieve water level from boundcond, otherwise param
    wl = boundcond.pop('BND_DILEV',param['ditch_level'])
    for k in boundcond.keys():   # use flux boundary conditions directly
        if 'BND' in k:
            bound[k] = boundcond[k]
        elif 'IN' in k:
            flux[k] = boundcond[k]
    
    # assert anisotropy correction:
    if 'ani' in param and 'ani_transformed' not in param:
        param = ani_transform(param)
    # Iteratively solve Qh relation
    statetm1 = state.copy()
    res = minimize_scalar(wbal,args=(flux,bound,wl,statetm1,param),tol=1e-5)
    state['head'] = res.x
    head_t = (state['head']+statetm1['head'])/2.
    flux['OUT_DR'],flux['OUT_DI'],state['mu_di'] = Qh(head_t,wl,param)
    flux.update(Boundaries(state['head'],bound,param))
    flux['STOR'] = -1000.*param['spec_yield']*(state['head']-statetm1['head']) # in mm
    
    # Evaluate zeta: how much of flow to drain / ditch is above / below zeta?
    # eq 24 from Groenendijk&Eertwegh 2004. Eq 24 calculates qy, but qy = 1 - integral(qx dy)
    # evaluation of eq 24 at y = zeta therefore gives total qx below zeta surface
    y = min(state['zeta'],state['head']) - state['head']
    fz_dr = (2./np.pi*np.arctan(np.exp(2.*np.pi*y/param['drain_L'])/
                (1-np.exp(4*np.pi*y/param['drain_L']))**0.5))
    fz_di = (2./np.pi*np.arctan(np.exp(2.*np.pi*y/param['ditch_L'])/
                (1-np.exp(4*np.pi*y/param['ditch_L']))**0.5))
    
    # fluxes above and below (are equal-sized..?)
    above = (flux['IN_P'] + (1.-fz_dr)*flux['OUT_DR'] + 
             (1.-fz_di)*flux['OUT_DI'] + flux['STOR'])
    below = flux['IN_SEEP'] + fz_dr*flux['OUT_DR'] + fz_di*flux['OUT_DI']
    state['zeta'] = min(state['head'],
                        state['zeta'] + below/1000./param['porosity'])
    
    conc = {'OUT_DR':np.nan,'OUT_DI':np.nan}
    flux['OUT_DI_INFIL'] = 0
    if flux['OUT_DR']:
        conc['OUT_DR'] = (((1.-fz_dr)*flux['OUT_DR']*param['precip_conc'] + 
                           fz_dr*flux['OUT_DR']*param['seep_conc']) / 
                          flux['OUT_DR'])
    
    # evaluate infiltrated water:
    # store infiltrated amount and infiltration height (wl at time of infiltration)
    if flux['OUT_DI'] > 0:
        state['infil'] += flux['OUT_DI']
        state['infil_height'] = ((flux['OUT_DI']*wl + 
                                  statetm1['infil']*statetm1['infil_height']) /
                                 state['infil'])
    elif state['infil'] > 0:
        y = min(state['infil_height'],state['head']) - state['head']
        fi_di = (2./np.pi*np.arctan(np.exp(2.*np.pi*y/param['ditch_L'])/
                    (1-np.exp(4*np.pi*y/param['ditch_L']))**0.5))
        flux['OUT_DI_INFIL'] = max(-state['infil'],fi_di*flux['OUT_DI'])
        state['infil'] += flux['OUT_DI_INFIL']
        
        # calculate remaining contribution of deep and shallow exf
        fiz = min(fz_di / fi_di,1.)
        flux_di_d = fz_di * flux['OUT_DI'] - fiz*flux['OUT_DI_INFIL']
        flux_di_s = (1-fz_di) * flux['OUT_DI'] - (1.-fiz)*flux['OUT_DI_INFIL']
        # determine concentration
        conc['OUT_DI'] = (flux['OUT_DI_INFIL']*param['infil_conc'] + 
                          flux_di_s*param['precip_conc'] + 
                          flux_di_d*param['seep_conc'])/flux['OUT_DI']
    else:
        conc['OUT_DI'] = (((1.-fz_di)*flux['OUT_DI']*param['precip_conc'] + 
                           fz_di*flux['OUT_DI']*param['seep_conc']) / 
                          flux['OUT_DI'])
    return flux,conc,state,statetm1

def run_rsgem(boundcond,state,param,verbose=False):
    '''Run RSGEM for all timesteps in boundcond,
    with starting_state state, and model parameters param
    
    Return pandas DataFrame'''
    if verbose: 
        t1 = time.time()
        print('Running RSGEM...',)
    # make sure no nodata in boundcond
    boundcond = boundcond.ffill()
    # apply anisotropy transformation
    param = ani_transform(param)
   
    # run model for consecutive time steps
    result = []
    for i,boundcond_t in boundcond.iterrows():
        flux,conc,state,statetm1 = model_tstep(boundcond=boundcond_t.to_dict(),
                                               state=state,
                                               param=param)
        result += [[state['head'],state['zeta'],state['infil'],state['mu_di'],
                    -flux['OUT_DR'],-flux['OUT_DI'],flux['IN_SEEP'],
                    flux['STOR'],conc['OUT_DR'],conc['OUT_DI']]]
    
    if verbose: 
        t2 = time.time()
        print(f'done, in {int(t2-t1)} sec')
    # return dataframe of results    
    return pd.DataFrame(result,index=boundcond.index,
                        columns=['head','zeta','infil','mu_di','qdr','qdi',
                                 'seep','stor','cqdr','cqdi'])
    

if __name__ == "__main__":
    import geopandas as gpd
    from pathlib import Path
    import imod
    # TODO: read shapefile with basins
    # read imod-wq input data and other data
    # average parameters per basin (k, c, ditch_level, drain_level, ani, area)

    # read imod-wq output data
    # average head and seepconcentration over basin

    path_input = Path("./data/1-external/rsgemimodwqtest")
    path_imodwq_results = path_input / "imodwq"
    path_lhminput = path_input / "lhm"
    path_lhmzzinput = path_input / "lhmzz"
    path_interim = Path("./data/2-interim/rsgemimodwqtest")
    path_interim.mkdir(exist_ok=True, parents=True)

    afwgebieden = gpd.read_file(path_input / "Waterbalansclusters_20240410.shp")
    afwgebieden = afwgebieden.loc[(afwgebieden.Hoofdclust == "Noordelijke kuststrook") & (afwgebieden.WS_BOEZEM == "Electraboezem 3e schil-Noord")]
    afwgebieden = afwgebieden[["GFEIDENT", "geometry"]]
    afwgebieden["code"] = range(1,len(afwgebieden)+1)
    afwgebieden.to_file(path_interim / "basins.gpkg")

    try:
        param = pd.read_csv(path_interim / "testparam.csv", index_col=0,header=0)
    except FileNotFoundError:
        lpf = xr.open_dataset(r"p:\archivedprojects\11206798-014-kpzss-verzilting\LHMzz-41_runs\data\2-interim\lpf.nc")
        laymodel = xr.open_dataset(r"p:\archivedprojects\11206798-014-kpzss-verzilting\LHMzz-41_runs\data\2-interim\layermodel.nc")
        k_horizontal = lpf["k_horizontal"].sel(layer=9).compute()  # holocene k
        c1 = (laymodel["dz"].sel(layer=10) / lpf["k_vertical"].sel(layer=10)).compute()
        # average over afwgebieden
        ditch_k = imod.prepare.zonal_aggregate_raster(path_interim / "basins.gpkg", column="code",raster=k_horizontal, resolution=250, method="mean")
        drain_k = ditch_k # maybe increase k in future?
        drain_L = 10.
        drain_d_ras = imod.rasterio.open(path_lhminput / "diepte_buisdrainage_lhm.tif")
        drain_level = imod.prepare.zonal_aggregate_raster(path_interim / "basins.gpkg", column="code",raster=drain_d_ras, resolution=250, method="mean")
        nopp = imod.rasterio.open(path_lhminput / "NHI_NOPP_2022_res_25m.tif")
        ditch_L = imod.prepare.zonal_aggregate_raster(path_interim / "basins.gpkg", column="code",raster=nopp, resolution=25, method="sum")
        ditch_L["ditch_L"] = ditch_L["area"] / np.fmax(1., ditch_L["nhi_nopp_2022_res_25m"])
        BND_SEEP_c = imod.prepare.zonal_aggregate_raster(path_interim / "basins.gpkg", column="code",raster=c1, resolution=250, method="mean")


        # put together in one Dataframe
        param = pd.concat([ditch_k["k_horizontal"], ditch_L["ditch_L"], drain_k["k_horizontal"], drain_level["diepte_buisdrainage_lhm"], BND_SEEP_c["aggregated"], ditch_L["area"]], axis=1, keys=["ditch_K","ditch_L","drain_K","drain_level","BND_SEEP_c","area"])
        # add additional parameters
        param["porosity"] = 0.35
        param["spec_yield"] =  .15
        param["ditch_level"] =  -1.0  # based on LHM
        param["ditch_bottom"] = param["ditch_level"] - .7
        param["drain_L"] = 10
        param["drain_b"] = .1
        param["ditch_b"] =  2.
        param["ditch_infil"] = False
        param["seep_conc"] = 25.  # TODO: based on model result
        param["precip_conc"] = 1.
        param["infil_conc"] = 2.
        param["ani"] = 10.  # TODO: based on kh / kv
        param["tstep"] = 86400.

        # not all basins have data for everything... 
        param = param.dropna(how="any")
        param.to_csv(path_interim / "testparam.csv")

    # model state          
    state = pd.DataFrame({'head': pd.Series(index=param.index, data=-1.),
            'zeta': pd.Series(index=param.index, data=-1.5),
            'infil': pd.Series(index=param.index, data=0),
            'infil_height': pd.Series(index=param.index, data=-1)
            })

    dr = pd.date_range("20200101","20210101",freq="D",name="time")
    boundcond = pd.DataFrame({"BND_SEEP":pd.Series(data=-1,index=dr), "IN_P":pd.Series(data=np.random.normal(0.5, 0.4, size=len(dr)),index=dr)})   # bnd_Seep met seep_c maakt samen cauchy boundary condition

    # just loop through cells
    results = {}
    for basin in param.index.values:
        results[basin] = run_rsgem(boundcond, state.loc[basin], param.loc[basin], verbose=True)
        results[basin]["qtot"] = results[basin]["qdr"].fillna(0) + results[basin]["qdi"].fillna(0)
        results[basin]["ctot"] = (results[basin]["qdr"].fillna(0) * results[basin]["cqdr"].fillna(0) + results[basin]["qdi"].fillna(0) * results[basin]["cqdi"].fillna(0)) / results[basin]["qtot"]

    
    df = pd.concat([pd.DataFrame({prm:results[basin][prm] for prm in ["head","qtot","ctot","seep"]}) for basin in results.keys()], axis=1, keys=[f"basin {k}" for k in results.keys()])
    print(df.head())
    df.to_csv("testresult.csv")
    df.plot(subplots=True)
    plt.show()


