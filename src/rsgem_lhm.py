"""
Adaptation of RSGEM model to work based on LHM fluxes

steps:
- Acquire water balance from LHM results for layer 1, consisting of:
  - bdgqmodfl -> recharge to saturated zone, from metaswap
  - bdgflf -> up/downward groundwater flow to layer 2
  - bdgriv, systems 1 - 6 -> drainage / infiltration to/from river systems
  - bdgdrn, systems 1 - 3? -> drainage to drainage systems
  and calculated groundwater level
- Also necessary: position of drain/riv level -> for solute concentration, not zeta per se?
- Apply RSGEM concepts to calculate position of fresh/salt interface and flux concentrations

How does RSGEM work again?
 eq 24 from Groenendijk&Eertwegh 2004. Eq 24 calculates qy, but qy = 1 - integral(qx dy)
 evaluation of eq 24 at y = zeta therefore gives total qx below zeta surface

first: get position of zeta, the fresh/salt interface, as min of zeta_t-1 and head
y = min(state['zeta'],state['head']) - state['head']

then: for each drainage system: calculate factor of flow that is below zeta
    fz_dr = (2./np.pi*np.arctan(np.exp(2.*np.pi*y/param['drain_L'])/
                (1-np.exp(4*np.pi*y/param['drain_L']))**0.5))
    fz_di = (2./np.pi*np.arctan(np.exp(2.*np.pi*y/param['ditch_L'])/
                (1-np.exp(4*np.pi*y/param['ditch_L']))**0.5))

finally: use these factors to divide waterbalance in part above and below zeta,
use difference to move zeta up and down



input dldra_svat.inp
6      1.52      1.39      1.00      1.49      0.15    281.29   1403.27   1000.00      9.43     18.86

==> derive from topsysteem!
  
"""
import numba
from numba import float32
import numpy as np
import pandas as pd
import xarray as xr
from pathlib import Path
from dask.diagnostics import ProgressBar
from dask.distributed import Client


### TEST: waarom klopt deze balans ook alweer niet?
# input_mf_t.isel(x=2,y=2,time=[-1]).to_dataframe().T
# input_ms.isel(x=2,y=2,time=[-1]).to_dataframe().T
# ---> ga uit van MODFLOW balans en assume verschil door recharge?

@numba.njit
def _calc_fz(y,L):
    return (2./np.pi*np.arctan(np.exp(2.*np.pi*y/L)/
                (1-np.exp(4*np.pi*y/L))**0.5))


@numba.njit
def _calculate_rsgem(zeta_start, head, bdgflf, fluxes, L, porosity, conc_rch, conc_gw, out_zeta, out_fluxwconc, out_fluxconc):
    nt,ny,nx = head.shape
    for iy in range(ny):
        for ix in range(nx):
            if np.isnan(conc_gw[iy, ix]) or conc_gw[iy, ix] < 0.1:
                continue
            for it in range(nt):
                if it:
                    zeta_tmin1 = out_zeta[it-1, iy, ix]
                else:
                    zeta_tmin1 = zeta_start[iy, ix]
                
                y = min(zeta_tmin1, head[it, iy, ix]) - head[it, iy, ix]
                fz = _calc_fz(y,L[:, iy, ix])
                below = bdgflf[it, iy, ix] + (fz*fluxes[:, it, iy, ix]).sum()
                out_zeta[it, iy, ix] = min(head[it, iy, ix], zeta_tmin1 + below / 1000. / porosity[iy, ix])
                out_fluxconc[:, it, iy, ix] = fz*conc_gw[iy, ix] + (1-fz)*conc_rch[iy, ix]
                out_fluxwconc[it, iy, ix] = (out_fluxconc[:, it, iy, ix] * fluxes[:, it, iy, ix]).sum() / fluxes[:, it, iy, ix].sum()


#@numba.guvectorize("(), (n), (n), (m,n), (m), (), (), (), () -> (n), (n), (m,n)")
@numba.guvectorize("(float32, float32[:], float32[:], float32[:,:], float32[:], float32, float32, float32, float32, float32[:], float32[:], float32[:,:])", "(), (n), (n), (m,n), (m), (), (), (), () -> (n), (n), (m,n)")
def _calculate_rsgem_guv(zeta_start, head, bdgflf, fluxes, L, porosity, conc_rch, conc_gw, min_zeta, out_zeta, out_fluxwconc, out_fluxconc):
    nt = int(head.shape[0])
    if np.isnan(conc_gw) or conc_gw < 0.1:
        return 
    for it in range(nt):
        if it:
            zeta_tmin1 = out_zeta[it-1]
        else:
            zeta_tmin1 = zeta_start
        
        y = min(zeta_tmin1, head[it]) - head[it]
        fz = _calc_fz(y,L)
        below = bdgflf[it] + (fz*fluxes[:, it]).sum()
        out_zeta[it] = min(head[it], zeta_tmin1 + below / 1000. / porosity)
        out_fluxconc[:, it] = fz*conc_gw + (1-fz)*conc_rch
        out_fluxwconc[it] = (out_fluxconc[:, it] * fluxes[:, it]).sum() / fluxes[:, it].sum()
        if out_zeta[it] < min_zeta:
            return
    return

def calculate_rsgem_lhm(input_mf, L, area, porosity, conc_rch, conc_gw):
    zeta = xr.full_like(input_mf["head"], np.nan).astype(np.float32)
    zeta.name = "zeta"
    fluxes = input_mf[["bdgriv_sys1","bdgriv_sys2","bdgriv_sys3","bdgdrn_sys1","bdgdrn_sys3"]].to_array(dim="sys",name="flux").chunk({"sys":5})
    fluxes = fluxes / area * 1000.   # m3 naar mm
    assert(L.shape[0]==fluxes.shape[0])
    fluxwconc = xr.full_like(input_mf["head"], np.nan).astype(np.float32)
    fluxwconc.name = "fluxwconc"
    fluxconc = xr.full_like(fluxes, np.nan).astype(np.float32)
    fluxconc.name = "conc"
    bdgflf = input_mf["bdgflf"] / area * 1000.   # m3 naar mm
    porosity = xr.full_like(input_mf["head"].isel(time=0), porosity)
    
    zeta_start = input_mf["head"].isel(time=0) - 2.5  # initialize zeta @ 2m below first head
    min_zeta = input_mf["head"].isel(time=0) - 30.
    #_calculate_rsgem(zeta_start, input_mf["head"].values, bdgflf.values, fluxes.values, L.values, porosity.values, conc_rch.values, conc_gw.values, zeta.values, fluxconc.values)
    zeta, fluxwconc, fluxconc = xr.apply_ufunc(_calculate_rsgem_guv, zeta_start.astype(np.float32), input_mf["head"].astype(np.float32), bdgflf.astype(np.float32), fluxes.astype(np.float32), L.astype(np.float32), porosity.astype(np.float32), conc_rch.astype(np.float32), conc_gw.astype(np.float32), min_zeta.astype(np.float32), zeta, fluxwconc, fluxconc, input_core_dims=[[],["time"],["time"],["sys","time"],["sys"],[],[],[],[],["time"],["time"],["sys","time"]], output_core_dims=[["time"],["time"],["sys","time"]],dask="parallelized",output_dtypes=[np.float32,np.float32,np.float32])  # list with one entry per arg
    return xr.Dataset({"zeta":zeta, "fluxwconc":fluxwconc, "fluxconc":fluxconc})

if __name__ == "__main__":

    #client = Client(n_workers=8)

    path_input = Path(".")
    fn_input_mf = path_input / "results_mf.nc"
    fn_input_mf2 = path_input / "results_mf_sys3.nc"
    fn_input_ms = path_input / "results_ms.nc"
    fn_dldrasvat = path_input / "dldra_svat.inp"
    fn_areasvat = path_input / "area_svat.inp"
    fn_solbnd = path_input / "solute_bnd.csv.inp"
    path_result = path_input / "result_rsgem_20112018.zarr"
    chunks = {"time":None, "y":100, "x":100}
    porosity = 0.4

    # load input
    input_mf = xr.open_dataset(fn_input_mf, chunks=chunks)#.isel(x=2,y=2)
    input_mf2 = xr.open_dataset(fn_input_mf2, chunks=chunks)
    input_mf = input_mf.update(input_mf2.isel(layer=0))
    input_ms = xr.open_dataset(fn_input_ms, chunks=chunks)#.isel(x=2,y=2)
    area_svat = pd.read_csv(fn_areasvat, delim_whitespace=True, header=None, names=["svat","area","elev","slk","lu","dprzk","nm","cfpm","cfE","x","y","_","irow","icol"])#, nrows=1000)
    dldra_svat = pd.read_csv(fn_dldrasvat, delim_whitespace=True, header=None, names=["svat"]+[f"D_s{i}" for i in range(1,6)]+[f"L_s{i}" for i in range(1,6)])#, nrows=1000)
    sol_bnd = pd.read_csv(fn_solbnd, delimiter=",", skiprows=2, header=None, names=["svat","conc_rain","conc_gw"])#, nrows=1000)

    ## correct sol_bnd
    def _corr(x):
        try:
            return float(x)
        except ValueError:
            return float(x.split()[0])

    sol_bnd["conc_gw"] = sol_bnd["conc_gw"].map(_corr)

    # use decadal time of metaswap, sum fluxes, for head use last
    # t = input_ms.time
    # t2 = t.reindex_like(input_mf.time,method="bfill")
    # input_mf_t = input_mf.groupby(t2).sum()
    # input_mf_t["head"] = input_mf["head"].reindex(time=t)

    # hdf = input_mf["head"].isel(time=0).to_dataframe()

    # from dataframes to datasets
    joined = area_svat.merge(dldra_svat, left_on="svat", right_on="svat").merge(sol_bnd, left_on="svat", right_on="svat")
    svat = joined.groupby(["y","x"]).first().to_xarray().reindex_like(input_mf)

    L = svat[[f"L_s{i}" for i in range(1,6)]].to_array(dim="sys", name="L").astype(float)
    L = L.assign_coords(sys=["bdgriv_sys1","bdgriv_sys2","bdgriv_sys3","bdgdrn_sys1","bdgdrn_sys3"])
    L2 = np.fmin(L,100.) # according to manual simgro?

    svatsel = svat.where(svat["conc_gw"] > 0.5)

    ds = calculate_rsgem_lhm(input_mf, L2, svatsel["area"], porosity, svatsel["conc_rain"], svatsel["conc_gw"])
    ds["head"] = input_mf["head"]
    ds["rwl_thick"] = input_mf["head"] - ds["zeta"]
    ds["min_rwl_thick"] = ds["rwl_thick"].min(dim="time")
    print(ds)
    with ProgressBar():
        ds.to_zarr(path_result)

    # fluxes.to_dataframe()["flux"].unstack().plot()
    # fluxconc.to_dataframe()["conc"].unstack().plot()
    # transol_load = input_ms["transol_bal_amdrru"]
    # flux_ms_tot = -input_mf_t[["bdgriv_sys1","bdgriv_sys2","bdgriv_sys3","bdgdrn_sys1","bdgdrn_sys3"]].to_array(dim="sys",name="flux").sum(dim="sys")
    # transol_conc = (transol_load * svat["area"]) / flux_ms_tot

    # fluxconc.sel(sys="flux_weighted").isel(x=2,y=2).plot(label="rsgem")
    # transol_conc.isel(x=2,y=2).plot(label="transol")
    # plt.legend()
    # plt.show()

    # transol_conc.plot()
    # conc_fw.plot()
    # input_mf["head"].plot()
    # zeta.plot()
    # plt.show()

