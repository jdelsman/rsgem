# RSGEM

RSGEM - Rapid Saline Groundwater Exfiltration Model

RSGEM is a simple rainfall-runoff model concept that models the outflow of fresh meteoric water and underlying saline groundwater to ditches and tile drainage.

Technical background is reported in: [Delsman, J.R., De Louw, P.G.B., De Lange, W.J., Oude Essink, G.H.P., 2017. Fast calculation of groundwater exfiltration salinity in a lowland catchment using a lumped celerity/velocity approach. Environ. Model. Softw. 96, 323–334.](http://dx.doi.org/10.1016/j.envsoft.2017.07.004) Please reference this publication when using RSGEM.
